import { Row, Col, Card, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function CourseCard({ course }) {

	// Checks to see if the data was successfully passed
	console.log(course);

	// Every component receives information in a form of an object
	console.log(typeof course);

	// Dexstructure the course prop from paren component Courses.js
	const { _id, name, description, price } = course;

	// Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
    // const [getter, setter] = useState(initialGetterValue);

	const [ count, setCount ] = useState(0);
	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

	console.log(useState(0));

/*	function enroll() {
		setCount(count + 1);
		console.log(`Enrollees : ${ count }`);
	}
*/
// activity s51
	
/*	const [ seatCount, setSeatCount ] = useState(30);

	function seat() {
			setSeatCount(seatCount - 1);
	}

	function clickSeatEnroll() {
		if (seatCount > 0) {
			enroll();
			seat();
		} else {
			alert(`No more seats`);
		};
	}}*/


// activity s51 solution

	const [seats, setSeats] = useState(30);

	console.log(useState(0));

	// function enroll(){
	//     if (seats > 0) {
	//         setCount(count + 1);
	//         // console.log('Enrollees: ' + count);
	//         setSeats(seats - 1);
	//         // console.log('Seats: ' + seats);
	//     } /*else {
	//         alert("No more seats available");
	//     };*/
	// }


// Use effect discussion

/*	useEffect(() => {
		if (seats === 0) {
			alert("no more seats available");
		}
	}, [seats])*/


	return (

/*		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h3>Sample Course</h3>
						</Card.Title>
						<Card.Text>
							<h6>Description:</h6>
							<p>This is a sample course offeing</p>
							<h6>Price:</h6>
							<p>PhP 40,000</p>
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
*/
		<Card className="mt-5">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP { price }</Card.Text>
                {/*<Card.Text>Enrollees : { count }</Card.Text>
                <Card.Text>Seats Available : { seats }</Card.Text>*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>


	)
}