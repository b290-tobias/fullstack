import { Col, Row } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import Home from '../pages/Home';

export default function PageNotFound() {

	return (

		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the <a href="/">homepage</a></p>
			</Col>
		</Row>
	)
}