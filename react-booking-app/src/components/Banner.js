import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom'
// Button, Col, and Row are folders under node modules react-bootstrap





export default function Banner() {



	return (

		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Link className="btn btn-primary" to="/courses">
					Enroll Now!
				</Link>
			</Col>
		</Row>
	)
}



/*
	- The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.
*/