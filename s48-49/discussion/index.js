// console.log(`s48-49`);


// fetch - to send request to make http requests
// data type being fetched - json
// fetch - to receive respone



// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
/*
	Syntax:
		fetch("url", {options})

			url - this is the address which the request is to be made and source where the response will come from (endpoint)
			options - array or properties that contains the HTTP method, body of request and headers.
*/



// Get Post Data / Retrieve / Read Function

fetch("https://jsonplaceholder.typicode.com/posts")
	.then(res => res.json())
	// .then(data => console.log(data));
	.then(data => showPosts(data))


// View Post - used to display each post from JSON Placeholder

const showPosts = posts => {

	//Create a variable that will contain all the posts
	let postEntries = "";

	// To iterate each posts from the API
	posts.forEach(post => {

		// to add again on the postEntries
		postEntries += `
			<div id="post-${post.id}">
				<h3 id=post-title-${post.id}>${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	// To check what is stored in the postEntries variables.
	// console.log(postEntries);


	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}



/*
	Mini-Activity:
		Retrieve a single post from JSON API and print it in the console.
*/


// to retrieve single post
fetch("https://jsonplaceholder.typicode.com/posts/50")
	.then(res => res.json())
	.then((data) => console.log(data));


fetch("https://jsonplaceholder.typicode.com/posts/25")
	.then(res => res.json())
	.then((data) => console.log(data));




// Post Data / Create Function
document.querySelector("#form-add-post").addEventListener("submit", e => {

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", 
		{
			method : "POST",
			body : JSON.stringify({
				title : document.querySelector("#txt-title").value,
				body : document.querySelector("#txt-body").value,
				userId : 290
			}),
			headers : {
				"Content-Type" : "application/json"
			}
		}).then(res => res.json())
			.then(data => {
				console.log(data);
				alert(`Post successfully added!`)
			});


	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;

});



// EDIT POST DATA / Edit Button
// triggered in <button onclick="editPost('${post.id}')">Edit</button>

const editPost = id => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body

	document.querySelector("#btn-submit-update").removeAttribute("disabled")
};



// Update Post Data / Put Method

document.querySelector("#form-edit-post").addEventListener("submit", e => {

	e.preventDefault();

	let id = document.querySelector(`#txt-edit-id`).value;

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, 
		{
			method : "PUT",
			body : JSON.stringify({
				id : id,

				// the value will be from the value of input fields or HTML elements
				title : document.querySelector("#txt-edit-title").value,
				body : document.querySelector("#txt-edit-body").value,
				userId : 290
			}),
			headers : {
				"Content-Type" : "application/json"
			}
		}).then(res => res.json())
			.then(data => {
				console.log(data);
				alert("Post succesfully updated!")
			});

	// Reset input fields once submitted
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;

	// Resetting disabled attribute for button
	document.querySelector("#btn-submit-update").setAttribute("disabled", true);
})


// Delete post



/*const deletePost = (id) => {

	let deleteId = document.querySelector(`#post-${id}`).value;

	fetch(`https://jsonplaceholder.typicode.com/posts/${deleteId}`, {method : "DELETE"})
		.then(res => {
			alert(`Post successfully deleted`)
			document.querySelector(`#post-${id}`).innerHTML = null;
		})
}
*/


/*const deletePost = id => {

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
		.then(res => res.json())
			.then(data => {
				console.log(data)
			})
}*/


const deletePost = id => {

	// let deleteId = document.querySelector(`#post-${id}`).value;

	// console.log(deleteId)

	// console.log(`https://jsonplaceholder.typicode.com/posts/${id}`)

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method : "DELETE"})
		.then(res => res.json())
			.then(data => {
			alert(`Post successfully deleted`);
			console.log(data);
		})

	document.querySelector(`#post-${id}`).remove()
}



// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
/*
	Syntax:
		fetch("url", {options})

			url - this is the address which the request is to be made and source where the response will come from (endpoint)
			options - array or properties that contains the HTTP method, body of request and headers.
*/
